import React, { Component } from "react";
import ItemList from "./ItemList";
import PropTypes from "prop-types";

class Posts extends Component {
  
  render() {
    return this.props.postlist.map(post => (
      <ItemList key={post.id} postitem={post} delTodo={this.props.delTodo} markCompleto={this.props.tigComplete} />
    ));
  }
}

Posts.propTypes = {
  postlist: PropTypes.array.isRequired
};

export default Posts;
