import React, { Component } from "react";
import PropTypes from "prop-types";

export class ItemList extends Component {
  getStyle = () => {
    return {
      background: "#f4f4f4",
      padding: "10px",
      borderBottom: "1px #ccc dotted",
      textDecoration: this.props.postitem.completed ? "line-through" : "none"
    };
  };

  // markComplete = () => {
  //   console.log(this.props);
  // };

  render() {
    const {id, title} = this.props.postitem;
    return (
      <div style={this.getStyle()}>
        <p>
          <input type="checkbox" onChange={this.props.markCompleto.bind(this, id)} />{" "}
          { title }
          <button style={btnStyle} onClick={this.props.delTodo.bind(this, id)}>x</button>
        </p>
      </div>
    );
  }
}

ItemList.propTypes = {
  postitem: PropTypes.object.isRequired
};

const btnStyle = {
  background: '#ff0000',
  color: '#fff',
  border: 'none',
  padding: '5px 10px',
  borderRadius: '50%',
  cursour: 'pointer',
  float: 'right'
}

export default ItemList;
