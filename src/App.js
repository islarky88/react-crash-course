import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from "./components/layout/Header";
import Posts from "./components/Posts";
import About from "./components/pages/About";
import "./App.css";

import uuid from "uuid";
import Axios from "axios";

// import Nav from "./components/Nav";
import AddPost from "./components/AddPost";

class App extends Component {
  state = {
    postlist: []
  };

  componentDidMount() {
    Axios.get("https://jsonplaceholder.typicode.com/todos?_limit=10").then(
      res => this.setState({ postlist: res.data })
    );
  }

  markComplete = id => {
    // console.log(id);
    this.setState({
      postlist: this.state.postlist.map(post => {
        if (post.id === id) {
          post.completed = !post.completed;
        }
        return post;
      })
    });
  };

  delTodo = id => {
    // console.log(id);
    this.setState({
      postlist: [...this.state.postlist.filter(postitem => postitem.id !== id)]
    });

    Axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then(res =>
      this.setState({ postlist: [...this.state.postlist.filter(post => post.id !== id)] })
    );

  };

  addPost = title => {
    Axios.post("https://jsonplaceholder.typicode.com/todos", {
      title: title,
      completed: false
    }).then(res =>
      this.setState({ postlist: [...this.state.postlist, res.data] })
    );
  };

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <Route
              exact
              path="/"
              render={props => (
                <React.Fragment>
                  <AddPost addPost={this.addPost} />

                  <Posts
                    postlist={this.state.postlist}
                    tigComplete={this.markComplete}
                    delTodo={this.delTodo}
                  />
                </React.Fragment>
              )}
            />

            <Route path="/about" component={About} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
